[![Finally a fast CMS](https://www.finally-a-fast.com/logos/logo-cms-readme.jpg)](https://www.finally-a-fast.com/) | Changelog | Module Youtube API
================================================

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Return types to attributeLabels, rules and prefixableTableName @cmoeke
- video_id to thumbnail filter @cmoeke
- Channel controller @cmoeke
- Model data links to menu @cmoeke 

### Changed
- Filter of existing videos to youtube_resource_id because youtube_id seems not to be unique @cmoeke
- Regenerated models @cmoeke
- Column sizes for fomantic @cmoeke
- Changed menu items @cmoeke

## [0.1.0] - 2020-09-29
### Added
- CHANGELOG.md @cmoeke fafcms-core#39

### Changed
- composer.json to use correct dependencies @cmoeke
- LICENSE to LICENSE.md @cmoeke
- PHP dependency to 7.4 @cmoeke
- Changed ci config to handle new build branch @cmoeke

### Fixed
- Broken README.md icons @cmoeke fafcms-core#46

[Unreleased]: https://gitlab.com/finally-a-fast/fafcms-module-youtube-api/-/tree/master
[0.1.0]: https://gitlab.com/finally-a-fast/fafcms-module-youtube-api/-/tree/v0.1.0-alpha
