<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-youtube-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/docs Documentation of fafcms-module-youtube-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\youtubeapi;

use fafcms\fafcms\components\FafcmsComponent;
use fafcms\youtubeapi\models\Channel;
use fafcms\youtubeapi\models\Thumbnail;
use fafcms\youtubeapi\models\Video;
use fafcms\youtubeapi\models\Youtubeuser;
use Yii;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;

/**
 * Class Bootstrap
 *
 * @package fafcms\youtubeapi
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-youtubeapi';
    public static $tablePrefix = 'fafcms-youtube_';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-youtubeapi'])) {
            $app->i18n->translations['fafcms-youtubeapi'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ .'/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapWebApp(Application $app, PluginModule $module): bool
    {
        Yii::$app->view->addNavigationItems(
            FafcmsComponent::NAVIGATION_SIDEBAR_LEFT,
            [
                'project' => [
                    'items' => [
                        'social'    => [
                            'after' => 'files',
                            'icon' => 'play-network-outline',
                            'label' => Yii::t('fafcms-core', 'Social media'),
                            'items' => [
                                'youtube' => [
                                    'icon' => 'youtube',
                                    'label' => Yii::t('fafcms-core', 'Youtube'),
                                    'items' => [
                                        'channel' => [
                                            'icon' => Channel::instance()->getEditData()['icon'],
                                            'label' => Channel::instance()->getEditData()['plural'],
                                            'url' => ['/' . Channel::instance()->getEditData()['url'] . '/index']
                                        ],
                                        'thumbnail' => [
                                            'icon' => Thumbnail::instance()->getEditData()['icon'],
                                            'label' => Thumbnail::instance()->getEditData()['plural'],
                                            'url' => ['/' . Thumbnail::instance()->getEditData()['url'] . '/index']
                                        ],
                                        'video' => [
                                            'icon' => Video::instance()->getEditData()['icon'],
                                            'label' => Video::instance()->getEditData()['plural'],
                                            'url' => ['/' . Video::instance()->getEditData()['url'] . '/index']
                                        ],
                                        'youtubeuser' => [
                                            'icon' => Youtubeuser::instance()->getEditData()['icon'],
                                            'label' => Youtubeuser::instance()->getEditData()['plural'],
                                            'url' => ['/' . Youtubeuser::instance()->getEditData()['url'] . '/index']
                                        ],
                                        'fetch' => [
                                            'icon' => 'download',
                                            'label' => 'Fetch data',
                                            'url' => ['/fafcms-youtubeapi/video/fetch']
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ]
        );

        $app->fafcms->addBackendUrlRules(self::$id, [
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>/<id:[a-zA-Z0-9\-\_]+>' => '<controller>/<action>',
            '<controller:[a-zA-Z0-9\-]+>/<action:[a-zA-Z0-9\-]+>' => '<controller>/<action>',
        ]);

        return true;
    }
}
