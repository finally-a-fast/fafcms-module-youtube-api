<?php

namespace fafcms\youtubeapi\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\youtubeapi\{
    Bootstrap,
    models\Channel,
    models\Thumbnail,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%video}}".
 *
 * @package fafcms\youtubeapi\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $channel_id
 * @property string|null $youtube_id
 * @property string|null $youtube_channel_id
 * @property string|null $youtube_playlist_id
 * @property string|null $youtube_resource_id
 * @property string|null $title
 * @property string|null $published_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property Channel $channel
 * @property Thumbnail[] $videoThumbnails
 */
abstract class BaseVideo extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/video';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'video';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Videos');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Video');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['title'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.title'
            ])
            ->setSort([static::tableName() . '.title' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'channel_id' => static function($properties = []) {
                return Channel::getOptionProvider($properties)->getOptions();
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'channel_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('channel_id', false),
                'relationClassName' => Channel::class,
            ],
            'youtube_id' => [
                'type' => TextInput::class,
            ],
            'youtube_channel_id' => [
                'type' => TextInput::class,
            ],
            'youtube_playlist_id' => [
                'type' => TextInput::class,
            ],
            'youtube_resource_id' => [
                'type' => TextInput::class,
            ],
            'title' => [
                'type' => TextInput::class,
            ],
            'published_at' => [
                'type' => DateTimePicker::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'channel_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'channel_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'youtube_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'youtube_id',
                        'sort' => 4,
                    ],
                ],
                'youtube_channel_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'youtube_channel_id',
                        'sort' => 5,
                    ],
                ],
                'youtube_playlist_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'youtube_playlist_id',
                        'sort' => 6,
                    ],
                ],
                'youtube_resource_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'youtube_resource_id',
                        'sort' => 7,
                    ],
                ],
                'published_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'published_at',
                        'sort' => 8,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-channel_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'channel_id',
                                                    ],
                                                ],
                                                'field-youtube_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'youtube_id',
                                                    ],
                                                ],
                                                'field-youtube_channel_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'youtube_channel_id',
                                                    ],
                                                ],
                                                'field-youtube_playlist_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'youtube_playlist_id',
                                                    ],
                                                ],
                                                'field-youtube_resource_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'youtube_resource_id',
                                                    ],
                                                ],
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-published_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'published_at',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%video}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-channel_id' => ['channel_id', 'required'],
            'integer-channel_id' => ['channel_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'date-published_at' => ['published_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'published_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-youtube_id' => ['youtube_id', 'string', 'max' => 255],
            'string-youtube_channel_id' => ['youtube_channel_id', 'string', 'max' => 255],
            'string-youtube_playlist_id' => ['youtube_playlist_id', 'string', 'max' => 255],
            'string-youtube_resource_id' => ['youtube_resource_id', 'string', 'max' => 255],
            'string-title' => ['title', 'string', 'max' => 255],
            'exist-channel_id' => [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Channel::class, 'targetAttribute' => ['channel_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-youtubeapi', 'ID'),
            'channel_id' => Yii::t('fafcms-youtubeapi', 'Channel ID'),
            'youtube_id' => Yii::t('fafcms-youtubeapi', 'Youtube ID'),
            'youtube_channel_id' => Yii::t('fafcms-youtubeapi', 'Youtube Channel ID'),
            'youtube_playlist_id' => Yii::t('fafcms-youtubeapi', 'Youtube Playlist ID'),
            'youtube_resource_id' => Yii::t('fafcms-youtubeapi', 'Youtube Resource ID'),
            'title' => Yii::t('fafcms-youtubeapi', 'Title'),
            'published_at' => Yii::t('fafcms-youtubeapi', 'Published At'),
            'created_by' => Yii::t('fafcms-youtubeapi', 'Created By'),
            'updated_by' => Yii::t('fafcms-youtubeapi', 'Updated By'),
            'activated_by' => Yii::t('fafcms-youtubeapi', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-youtubeapi', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-youtubeapi', 'Deleted By'),
            'created_at' => Yii::t('fafcms-youtubeapi', 'Created At'),
            'updated_at' => Yii::t('fafcms-youtubeapi', 'Updated At'),
            'activated_at' => Yii::t('fafcms-youtubeapi', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-youtubeapi', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-youtubeapi', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[Channel]].
     *
     * @return ActiveQuery
     */
    public function getChannel(): ActiveQuery
    {
        return $this->hasOne(Channel::class, [
            'id' => 'channel_id',
        ]);
    }

    /**
     * Gets query for [[VideoThumbnails]].
     *
     * @return ActiveQuery
     */
    public function getVideoThumbnails(): ActiveQuery
    {
        return $this->hasMany(Thumbnail::class, [
            'video_id' => 'id',
        ]);
    }
}
