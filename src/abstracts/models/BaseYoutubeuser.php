<?php

namespace fafcms\youtubeapi\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\youtubeapi\{
    Bootstrap,
    models\Channel,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%youtubeuser}}".
 *
 * @package fafcms\youtubeapi\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property string $status
 * @property string $name
 * @property string|null $remarks
 * @property int $download_thumbnails
 * @property string|null $thumbnail_path
 * @property string|null $key
 * @property string $schedule
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property Channel[] $youtubeuserChannels
 */
abstract class BaseYoutubeuser extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/youtubeuser';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'youtubeuser';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Youtubeusers');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Youtubeuser');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['name'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.name'
            ])
            ->setSort([static::tableName() . '.name' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'name' => [
                'type' => TextInput::class,
            ],
            'remarks' => [
                'type' => Textarea::class,
            ],
            'download_thumbnails' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'thumbnail_path' => [
                'type' => TextInput::class,
            ],
            'key' => [
                'type' => TextInput::class,
            ],
            'schedule' => [
                'type' => TextInput::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'name' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'name',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'remarks' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'remarks',
                        'sort' => 4,
                    ],
                ],
                'download_thumbnails' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'download_thumbnails',
                        'sort' => 5,
                    ],
                ],
                'thumbnail_path' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'thumbnail_path',
                        'sort' => 6,
                    ],
                ],
                'key' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'key',
                        'sort' => 7,
                    ],
                ],
                'schedule' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'schedule',
                        'sort' => 8,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-name' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'name',
                                                    ],
                                                ],
                                                'field-remarks' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'remarks',
                                                    ],
                                                ],
                                                'field-download_thumbnails' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'download_thumbnails',
                                                    ],
                                                ],
                                                'field-thumbnail_path' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'thumbnail_path',
                                                    ],
                                                ],
                                                'field-key' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'key',
                                                    ],
                                                ],
                                                'field-schedule' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'schedule',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%youtubeuser}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-name' => ['name', 'required'],
            'string-remarks' => ['remarks', 'string'],
            'boolean-download_thumbnails' => ['download_thumbnails', 'boolean'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-name' => ['name', 'string', 'max' => 255],
            'string-thumbnail_path' => ['thumbnail_path', 'string', 'max' => 255],
            'string-key' => ['key', 'string', 'max' => 255],
            'string-schedule' => ['schedule', 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-youtubeapi', 'ID'),
            'status' => Yii::t('fafcms-youtubeapi', 'Status'),
            'name' => Yii::t('fafcms-youtubeapi', 'Name'),
            'remarks' => Yii::t('fafcms-youtubeapi', 'Remarks'),
            'download_thumbnails' => Yii::t('fafcms-youtubeapi', 'Download Thumbnails'),
            'thumbnail_path' => Yii::t('fafcms-youtubeapi', 'Thumbnail Path'),
            'key' => Yii::t('fafcms-youtubeapi', 'Key'),
            'schedule' => Yii::t('fafcms-youtubeapi', 'Schedule'),
            'created_by' => Yii::t('fafcms-youtubeapi', 'Created By'),
            'updated_by' => Yii::t('fafcms-youtubeapi', 'Updated By'),
            'activated_by' => Yii::t('fafcms-youtubeapi', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-youtubeapi', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-youtubeapi', 'Deleted By'),
            'created_at' => Yii::t('fafcms-youtubeapi', 'Created At'),
            'updated_at' => Yii::t('fafcms-youtubeapi', 'Updated At'),
            'activated_at' => Yii::t('fafcms-youtubeapi', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-youtubeapi', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-youtubeapi', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[YoutubeuserChannels]].
     *
     * @return ActiveQuery
     */
    public function getYoutubeuserChannels(): ActiveQuery
    {
        return $this->hasMany(Channel::class, [
            'youtubeuser_id' => 'id',
        ]);
    }
}
