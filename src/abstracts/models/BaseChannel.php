<?php

namespace fafcms\youtubeapi\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\DropDownList,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\SwitchCheckbox,
    inputs\Textarea,
    inputs\TextInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\youtubeapi\{
    Bootstrap,
    models\Thumbnail,
    models\Video,
    models\Youtubeuser,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%channel}}".
 *
 * @package fafcms\youtubeapi\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int $youtubeuser_id
 * @property string $status
 * @property string|null $youtube_id
 * @property string $title
 * @property string|null $description
 * @property string|null $country
 * @property string|null $custom_url
 * @property string|null $default_language
 * @property string|null $published_at
 * @property string|null $favorites
 * @property string|null $likes
 * @property string|null $uploads
 * @property string|null $watch_history
 * @property string|null $watch_later
 * @property int $download_thumbnails
 * @property string|null $thumbnail_path
 * @property string $schedule
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property Thumbnail[] $channelThumbnails
 * @property Video[] $channelVideos
 * @property Youtubeuser $youtubeuser
 */
abstract class BaseChannel extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/channel';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'channel';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Channels');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Channel');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['title'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.title'
            ])
            ->setSort([static::tableName() . '.title' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'youtubeuser_id' => static function($properties = []) {
                return Youtubeuser::getOptionProvider($properties)->getOptions();
            },
            'status' => [
                static::STATUS_ACTIVE => Yii::t('fafcms-core', 'Active'),
                static::STATUS_INACTIVE => Yii::t('fafcms-core', 'Inactive'),
            ],
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'youtubeuser_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('youtubeuser_id', false),
                'relationClassName' => Youtubeuser::class,
            ],
            'status' => [
                'type' => DropDownList::class,
                'items' => $this->getAttributeOptions('status', false),
            ],
            'youtube_id' => [
                'type' => TextInput::class,
            ],
            'title' => [
                'type' => TextInput::class,
            ],
            'description' => [
                'type' => Textarea::class,
            ],
            'country' => [
                'type' => TextInput::class,
            ],
            'custom_url' => [
                'type' => UrlInput::class,
            ],
            'default_language' => [
                'type' => TextInput::class,
            ],
            'published_at' => [
                'type' => DateTimePicker::class,
            ],
            'favorites' => [
                'type' => TextInput::class,
            ],
            'likes' => [
                'type' => TextInput::class,
            ],
            'uploads' => [
                'type' => TextInput::class,
            ],
            'watch_history' => [
                'type' => TextInput::class,
            ],
            'watch_later' => [
                'type' => TextInput::class,
            ],
            'download_thumbnails' => [
                'type' => SwitchCheckbox::class,
                'offLabel' => [
                    'fafcms-core',
                    'No',
                ],
                'onLabel' => [
                    'fafcms-core',
                    'Yes',
                ],
            ],
            'thumbnail_path' => [
                'type' => TextInput::class,
            ],
            'schedule' => [
                'type' => TextInput::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                    ],
                ],
                'status' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'status',
                        'sort' => 2,
                    ],
                ],
                'title' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'title',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'youtubeuser_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'youtubeuser_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'youtube_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'youtube_id',
                        'sort' => 5,
                    ],
                ],
                'description' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'description',
                        'sort' => 6,
                    ],
                ],
                'country' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'country',
                        'sort' => 7,
                    ],
                ],
                'custom_url' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'custom_url',
                        'sort' => 8,
                    ],
                ],
                'default_language' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'default_language',
                        'sort' => 9,
                    ],
                ],
                'published_at' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'published_at',
                        'sort' => 10,
                    ],
                ],
                'favorites' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'favorites',
                        'sort' => 11,
                    ],
                ],
                'likes' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'likes',
                        'sort' => 12,
                    ],
                ],
                'uploads' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'uploads',
                        'sort' => 13,
                    ],
                ],
                'watch_history' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'watch_history',
                        'sort' => 14,
                    ],
                ],
                'watch_later' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'watch_later',
                        'sort' => 15,
                    ],
                ],
                'download_thumbnails' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'download_thumbnails',
                        'sort' => 16,
                    ],
                ],
                'thumbnail_path' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'thumbnail_path',
                        'sort' => 17,
                    ],
                ],
                'schedule' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'schedule',
                        'sort' => 18,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-youtubeuser_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'youtubeuser_id',
                                                    ],
                                                ],
                                                'field-status' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'status',
                                                    ],
                                                ],
                                                'field-youtube_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'youtube_id',
                                                    ],
                                                ],
                                                'field-title' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'title',
                                                    ],
                                                ],
                                                'field-description' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'description',
                                                    ],
                                                ],
                                                'field-country' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'country',
                                                    ],
                                                ],
                                                'field-custom_url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'custom_url',
                                                    ],
                                                ],
                                                'field-default_language' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'default_language',
                                                    ],
                                                ],
                                                'field-published_at' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'published_at',
                                                    ],
                                                ],
                                                'field-favorites' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'favorites',
                                                    ],
                                                ],
                                                'field-likes' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'likes',
                                                    ],
                                                ],
                                                'field-uploads' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'uploads',
                                                    ],
                                                ],
                                                'field-watch_history' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'watch_history',
                                                    ],
                                                ],
                                                'field-watch_later' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'watch_later',
                                                    ],
                                                ],
                                                'field-download_thumbnails' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'download_thumbnails',
                                                    ],
                                                ],
                                                'field-thumbnail_path' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'thumbnail_path',
                                                    ],
                                                ],
                                                'field-schedule' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'schedule',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%channel}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'required-youtubeuser_id' => ['youtubeuser_id', 'required'],
            'required-title' => ['title', 'required'],
            'integer-youtubeuser_id' => ['youtubeuser_id', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'string-description' => ['description', 'string'],
            'date-published_at' => ['published_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'published_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'boolean-download_thumbnails' => ['download_thumbnails', 'boolean'],
            'string-status' => ['status', 'string', 'max' => 255],
            'string-youtube_id' => ['youtube_id', 'string', 'max' => 255],
            'string-title' => ['title', 'string', 'max' => 255],
            'string-country' => ['country', 'string', 'max' => 255],
            'string-custom_url' => ['custom_url', 'string', 'max' => 255],
            'string-default_language' => ['default_language', 'string', 'max' => 255],
            'string-favorites' => ['favorites', 'string', 'max' => 255],
            'string-likes' => ['likes', 'string', 'max' => 255],
            'string-uploads' => ['uploads', 'string', 'max' => 255],
            'string-watch_history' => ['watch_history', 'string', 'max' => 255],
            'string-watch_later' => ['watch_later', 'string', 'max' => 255],
            'string-thumbnail_path' => ['thumbnail_path', 'string', 'max' => 255],
            'string-schedule' => ['schedule', 'string', 'max' => 255],
            'exist-youtubeuser_id' => [['youtubeuser_id'], 'exist', 'skipOnError' => true, 'targetClass' => Youtubeuser::class, 'targetAttribute' => ['youtubeuser_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-youtubeapi', 'ID'),
            'youtubeuser_id' => Yii::t('fafcms-youtubeapi', 'Youtubeuser ID'),
            'status' => Yii::t('fafcms-youtubeapi', 'Status'),
            'youtube_id' => Yii::t('fafcms-youtubeapi', 'Youtube ID'),
            'title' => Yii::t('fafcms-youtubeapi', 'Title'),
            'description' => Yii::t('fafcms-youtubeapi', 'Description'),
            'country' => Yii::t('fafcms-youtubeapi', 'Country'),
            'custom_url' => Yii::t('fafcms-youtubeapi', 'Custom Url'),
            'default_language' => Yii::t('fafcms-youtubeapi', 'Default Language'),
            'published_at' => Yii::t('fafcms-youtubeapi', 'Published At'),
            'favorites' => Yii::t('fafcms-youtubeapi', 'Favorites'),
            'likes' => Yii::t('fafcms-youtubeapi', 'Likes'),
            'uploads' => Yii::t('fafcms-youtubeapi', 'Uploads'),
            'watch_history' => Yii::t('fafcms-youtubeapi', 'Watch History'),
            'watch_later' => Yii::t('fafcms-youtubeapi', 'Watch Later'),
            'download_thumbnails' => Yii::t('fafcms-youtubeapi', 'Download Thumbnails'),
            'thumbnail_path' => Yii::t('fafcms-youtubeapi', 'Thumbnail Path'),
            'schedule' => Yii::t('fafcms-youtubeapi', 'Schedule'),
            'created_by' => Yii::t('fafcms-youtubeapi', 'Created By'),
            'updated_by' => Yii::t('fafcms-youtubeapi', 'Updated By'),
            'activated_by' => Yii::t('fafcms-youtubeapi', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-youtubeapi', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-youtubeapi', 'Deleted By'),
            'created_at' => Yii::t('fafcms-youtubeapi', 'Created At'),
            'updated_at' => Yii::t('fafcms-youtubeapi', 'Updated At'),
            'activated_at' => Yii::t('fafcms-youtubeapi', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-youtubeapi', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-youtubeapi', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[ChannelThumbnails]].
     *
     * @return ActiveQuery
     */
    public function getChannelThumbnails(): ActiveQuery
    {
        return $this->hasMany(Thumbnail::class, [
            'channel_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[ChannelVideos]].
     *
     * @return ActiveQuery
     */
    public function getChannelVideos(): ActiveQuery
    {
        return $this->hasMany(Video::class, [
            'channel_id' => 'id',
        ]);
    }

    /**
     * Gets query for [[Youtubeuser]].
     *
     * @return ActiveQuery
     */
    public function getYoutubeuser(): ActiveQuery
    {
        return $this->hasOne(Youtubeuser::class, [
            'id' => 'youtubeuser_id',
        ]);
    }
}
