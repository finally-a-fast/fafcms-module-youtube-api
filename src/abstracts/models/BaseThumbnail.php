<?php

namespace fafcms\youtubeapi\abstracts\models;

use fafcms\fafcms\{
    inputs\DateTimePicker,
    inputs\ExtendedDropDownList,
    inputs\NumberInput,
    inputs\UrlInput,
    items\ActionColumn,
    items\Card,
    items\Column,
    items\DataColumn,
    items\FormField,
    items\Row,
    items\Tab,
};
use fafcms\filemanager\{
    inputs\FileSelect,
    models\File,
};
use fafcms\helpers\{
    ActiveRecord,
    classes\OptionProvider,
    interfaces\EditViewInterface,
    interfaces\FieldConfigInterface,
    interfaces\IndexViewInterface,
    traits\AttributeOptionTrait,
    traits\BeautifulModelTrait,
    traits\OptionProviderTrait,
};
use fafcms\youtubeapi\{
    Bootstrap,
    models\Channel,
    models\Video,
};
use Yii;
use yii\db\ActiveQuery;
use yii\validators\DateValidator;

/**
 * This is the abstract model class for table "{{%thumbnail}}".
 *
 * @package fafcms\youtubeapi\abstracts\models
 *
 * @property-read array $fieldConfig
 *
 * @property int $id
 * @property int|null $channel_id
 * @property int|null $video_id
 * @property int|null $file_id
 * @property string|null $url
 * @property int|null $height
 * @property int|null $width
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $activated_by
 * @property int|null $deactivated_by
 * @property int|null $deleted_by
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $activated_at
 * @property string|null $deactivated_at
 * @property string|null $deleted_at
 *
 * @property Channel $channel
 * @property File $file
 * @property Video $video
 */
abstract class BaseThumbnail extends ActiveRecord implements FieldConfigInterface, IndexViewInterface, EditViewInterface
{
    use BeautifulModelTrait;
    use OptionProviderTrait;
    use AttributeOptionTrait;

    //region BeautifulModelTrait implementation
    /**
     * @inheritDoc
     */
    public static function editDataUrl($model): string
    {
        return Bootstrap::$id . '/thumbnail';
    }

    /**
     * @inheritDoc
     */
    public static function editDataIcon($model): string
    {
        return  'thumbnail';
    }

    /**
     * @inheritDoc
     */
    public static function editDataPlural($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Thumbnails');
    }

    /**
     * @inheritDoc
     */
    public static function editDataSingular($model): string
    {
        return Yii::t('fafcms-youtubeapi', 'Thumbnail');
    }

    /**
     * @inheritDoc
     */
    public static function extendedLabel($model, bool $html = true, array $params = []): string
    {
        return trim(($model['id'] ?? ''));
    }
    //endregion BeautifulModelTrait implementation

    //region OptionProviderTrait implementation
    /**
     * @inheritDoc
     */
    public static function getOptionProvider(array $properties = []): OptionProvider
    {
        return (new OptionProvider(static::class))
            ->setSelect([
                static::tableName() . '.id',
                static::tableName() . '.id'
            ])
            ->setSort([static::tableName() . '.id' => SORT_ASC])
            ->setItemLabel(static function ($item) {
                return static::extendedLabel($item);
            })
            ->setProperties($properties);
    }
    //endregion OptionProviderTrait implementation

    //region AttributeOptionTrait implementation
    /**
     * @inheritDoc
     */
    public function attributeOptions(): array
    {
        return [
            'channel_id' => static function($properties = []) {
                return Channel::getOptionProvider($properties)->getOptions();
            },
            'video_id' => static function($properties = []) {
                return Video::getOptionProvider($properties)->getOptions();
            },
            'file_id' => static function(...$params) {
                return File::getOptions(...$params);
            },
        ];
    }
    //endregion AttributeOptionTrait implementation

    //region FieldConfigInterface implementation
    public function getFieldConfig(): array
    {
        return [
            'id' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'channel_id' => [
                'type' => ExtendedDropDownList::class,
                'items' => $this->getAttributeOptions('channel_id', false),
                'relationClassName' => Channel::class,
            ],
            'video_id' => [
                'type' => FileSelect::class,
                'mediatypes' => [
                    'video',
                ],
                'external' => false,
            ],
            'file_id' => [
                'type' => FileSelect::class,
                'external' => false,
            ],
            'url' => [
                'type' => UrlInput::class,
            ],
            'height' => [
                'type' => NumberInput::class,
            ],
            'width' => [
                'type' => NumberInput::class,
            ],
            'created_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_by' => [
                'type' => NumberInput::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'created_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'updated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'activated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deactivated_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
            'deleted_at' => [
                'type' => DateTimePicker::class,
                'options' => [
                    'disabled' => true,
                ],
            ],
        ];
    }
    //endregion FieldConfigInterface implementation

    //region IndexViewInterface implementation
    public static function indexView(): array
    {
        return [
            'default' => [
                'id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'id',
                        'sort' => 1,
                        'link' => true,
                    ],
                ],
                'channel_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'channel_id',
                        'sort' => 2,
                        'link' => true,
                    ],
                ],
                'video_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'video_id',
                        'sort' => 3,
                        'link' => true,
                    ],
                ],
                'file_id' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'file_id',
                        'sort' => 4,
                        'link' => true,
                    ],
                ],
                'url' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'url',
                        'sort' => 5,
                    ],
                ],
                'height' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'height',
                        'sort' => 6,
                    ],
                ],
                'width' => [
                    'class' => DataColumn::class,
                    'settings' => [
                        'field' => 'width',
                        'sort' => 7,
                    ],
                ],
                'action-column' => [
                    'class' => ActionColumn::class,
                ],
            ]
        ];
    }
    //endregion IndexViewInterface implementation

    //region EditViewInterface implementation
    public static function editView(): array
    {
        return [
            'default' => [
                'tab-1' => [
                    'class' => Tab::class,
                    'settings' => [
                        'label' => [
                            'fafcms-core',
                            'Master data',
                        ],
                    ],
                    'contents' => [
                        'row-1' => [
                            'class' => Row::class,
                            'contents' => [
                                'column-1' => [
                                    'class' => Column::class,
                                    'settings' => [
                                        'm' => 8,
                                    ],
                                    'contents' => [
                                        'card-1' => [
                                            'class' => Card::class,
                                            'settings' => [
                                                'title' => [
                                                    'fafcms-core',
                                                    'Master data',
                                                ],
                                                'icon' => 'playlist-edit',
                                            ],
                                            'contents' => [
                                                'field-channel_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'channel_id',
                                                    ],
                                                ],
                                                'field-video_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'video_id',
                                                    ],
                                                ],
                                                'field-file_id' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'file_id',
                                                    ],
                                                ],
                                                'field-url' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'url',
                                                    ],
                                                ],
                                                'field-height' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'height',
                                                    ],
                                                ],
                                                'field-width' => [
                                                    'class' => FormField::class,
                                                    'settings' => [
                                                        'field' => 'width',
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
    //endregion EditViewInterface implementation

    /**
     * {@inheritdoc}
     */
    public static function prefixableTableName(): string
    {
        return '{{%thumbnail}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            'integer-channel_id' => ['channel_id', 'integer'],
            'integer-video_id' => ['video_id', 'integer'],
            'integer-file_id' => ['file_id', 'integer'],
            'integer-height' => ['height', 'integer'],
            'integer-width' => ['width', 'integer'],
            'integer-created_by' => ['created_by', 'integer'],
            'integer-updated_by' => ['updated_by', 'integer'],
            'integer-activated_by' => ['activated_by', 'integer'],
            'integer-deactivated_by' => ['deactivated_by', 'integer'],
            'integer-deleted_by' => ['deleted_by', 'integer'],
            'date-created_at' => ['created_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'created_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-updated_at' => ['updated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'updated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-activated_at' => ['activated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'activated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deactivated_at' => ['deactivated_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deactivated_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'date-deleted_at' => ['deleted_at', 'date', 'type' => DateValidator::TYPE_DATETIME, 'timestampAttribute' => 'deleted_at', 'timestampAttributeFormat' => 'php:Y-m-d H:i:s'],
            'string-url' => ['url', 'string', 'max' => 255],
            'exist-channel_id' => [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => Channel::class, 'targetAttribute' => ['channel_id' => 'id']],
            'exist-file_id' => [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::class, 'targetAttribute' => ['file_id' => 'id']],
            'exist-video_id' => [['video_id'], 'exist', 'skipOnError' => true, 'targetClass' => Video::class, 'targetAttribute' => ['video_id' => 'id']],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return array_merge(parent::attributeLabels(), [
            'id' => Yii::t('fafcms-youtubeapi', 'ID'),
            'channel_id' => Yii::t('fafcms-youtubeapi', 'Channel ID'),
            'video_id' => Yii::t('fafcms-youtubeapi', 'Video ID'),
            'file_id' => Yii::t('fafcms-youtubeapi', 'File ID'),
            'url' => Yii::t('fafcms-youtubeapi', 'Url'),
            'height' => Yii::t('fafcms-youtubeapi', 'Height'),
            'width' => Yii::t('fafcms-youtubeapi', 'Width'),
            'created_by' => Yii::t('fafcms-youtubeapi', 'Created By'),
            'updated_by' => Yii::t('fafcms-youtubeapi', 'Updated By'),
            'activated_by' => Yii::t('fafcms-youtubeapi', 'Activated By'),
            'deactivated_by' => Yii::t('fafcms-youtubeapi', 'Deactivated By'),
            'deleted_by' => Yii::t('fafcms-youtubeapi', 'Deleted By'),
            'created_at' => Yii::t('fafcms-youtubeapi', 'Created At'),
            'updated_at' => Yii::t('fafcms-youtubeapi', 'Updated At'),
            'activated_at' => Yii::t('fafcms-youtubeapi', 'Activated At'),
            'deactivated_at' => Yii::t('fafcms-youtubeapi', 'Deactivated At'),
            'deleted_at' => Yii::t('fafcms-youtubeapi', 'Deleted At'),
        ]);
    }

    /**
     * Gets query for [[Channel]].
     *
     * @return ActiveQuery
     */
    public function getChannel(): ActiveQuery
    {
        return $this->hasOne(Channel::class, [
            'id' => 'channel_id',
        ]);
    }

    /**
     * Gets query for [[File]].
     *
     * @return ActiveQuery
     */
    public function getFile(): ActiveQuery
    {
        return $this->hasOne(File::class, [
            'id' => 'file_id',
        ]);
    }

    /**
     * Gets query for [[Video]].
     *
     * @return ActiveQuery
     */
    public function getVideo(): ActiveQuery
    {
        return $this->hasOne(Video::class, [
            'id' => 'video_id',
        ]);
    }
}
