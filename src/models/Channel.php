<?php

namespace fafcms\youtubeapi\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\youtubeapi\abstracts\models\BaseChannel;
use fafcms\youtubeapi\Bootstrap;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%channel}}".
 *
 * @package fafcms\youtubeapi\models
 */
class Channel extends BaseChannel
{
    use MultilingualTrait;
}
