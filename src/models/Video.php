<?php

namespace fafcms\youtubeapi\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\youtubeapi\abstracts\models\BaseVideo;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%video}}".
 *
 * @package fafcms\youtubeapi\models
 */
class Video extends BaseVideo
{
    use MultilingualTrait;
}
