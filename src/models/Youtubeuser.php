<?php

namespace fafcms\youtubeapi\models;

use fafcms\helpers\traits\MultilingualTrait;
use fafcms\youtubeapi\abstracts\models\BaseYoutubeuser;

/**
 * This is the model class for table "{{%youtubeuser}}".
 *
 * @package fafcms\youtubeapi\models
 */
class Youtubeuser extends BaseYoutubeuser
{
    use MultilingualTrait;
}
