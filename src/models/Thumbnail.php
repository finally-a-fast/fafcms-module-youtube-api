<?php

namespace fafcms\youtubeapi\models;

use fafcms\helpers\traits\MultilingualTrait;
use Google_Service_YouTube_ThumbnailDetails;
use Google_Service_YouTube_Thumbnail;
use fafcms\youtubeapi\abstracts\models\BaseThumbnail;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%thumbnail}}".
 *
 * @package fafcms\youtubeapi\models
 */
class Thumbnail extends BaseThumbnail
{
    use MultilingualTrait;

    /**
     * @param Google_Service_YouTube_ThumbnailDetails $thumbnailDetails
     *
     * @return Google_Service_YouTube_Thumbnail|null
     */
    public static function getBestResulutionThumbnail(Google_Service_YouTube_ThumbnailDetails $thumbnailDetails): ?Google_Service_YouTube_Thumbnail
    {
        if (isset($thumbnailDetails->maxres)) {
            return $thumbnailDetails->maxres;
        }

        if (isset($thumbnailDetails->standard)) {
            return $thumbnailDetails->standard;
        }

        if (isset($thumbnailDetails->high)) {
            return $thumbnailDetails->high;
        }

        if (isset($thumbnailDetails->medium)) {
            return $thumbnailDetails->medium;
        }

        if (isset($thumbnailDetails->default)) {
            return $thumbnailDetails->default;
        }

        return null;
    }
}
