<?php

namespace fafcms\youtubeapi\controllers;

use fafcms\helpers\DefaultController;
use fafcms\youtubeapi\models\Channel;

/**
 * Class ChannelController
 *
 * @package fafcms\sitemanager\controllers
 */
class ChannelController extends DefaultController
{
    public static $modelClass = Channel::class;
}
