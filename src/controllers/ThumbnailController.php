<?php

namespace fafcms\youtubeapi\controllers;

use fafcms\helpers\DefaultController;
use fafcms\youtubeapi\models\Thumbnail;

/**
 * Class ThumbnailController
 *
 * @package fafcms\youtubeapi\controllers
 */
class ThumbnailController extends DefaultController
{
    public static $modelClass = Thumbnail::class;
}
