<?php

namespace fafcms\youtubeapi\controllers;

use fafcms\helpers\DefaultController;
use fafcms\youtubeapi\models\Youtubeuser;

/**
 * Class YoutubeuserController
 *
 * @package fafcms\youtubeapi\controllers
 */
class YoutubeuserController extends DefaultController
{
    public static $modelClass = Youtubeuser::class;
}
