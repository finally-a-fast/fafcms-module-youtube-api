<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-youtube-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/docs Documentation of fafcms-module-youtube-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\youtubeapi\controllers;

use fafcms\fafcms\models\QueueHelper;
use fafcms\helpers\DefaultController;
use fafcms\youtubeapi\jobs\GetChannelsJob;
use fafcms\youtubeapi\jobs\GetVideosJob;
use fafcms\youtubeapi\models\Channel;
use fafcms\youtubeapi\models\Video;
use Yii;
use yii\web\Response;

/**
 * Class VideoController
 *
 * @package fafcms\youtubeapi\controllers
 */
class VideoController extends DefaultController
{
    public static $modelClass = Video::class;

    /**
     * @return \yii\web\Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionFetch(): Response
    {
        foreach (Channel::find()->byProject('all')->byProjectLanguage('all')->all() as $channel) {
            QueueHelper::runJob(GetVideosJob::class, [
                'channelId' => $channel->id,
            ]);
        }

        Yii::$app->session->setFlash('success', Yii::t('fafcms-youtubeapi', 'Added job for Video download.'));

        return $this->goBack(Yii::$app->getRequest()->getReferrer());
    }
}

