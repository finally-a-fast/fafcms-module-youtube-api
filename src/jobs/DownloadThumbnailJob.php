<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-youtube-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/docs Documentation of fafcms-module-youtube-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\youtubeapi\jobs;

use fafcms\filemanager\models\File;
use fafcms\filemanager\models\Filetype;
use fafcms\filemanager\Module;
use fafcms\youtubeapi\models\Thumbnail;
use Yii;
use GuzzleHttp\Client;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class DownloadThumbnailJob
 *
 * @package fafcms\youtubeapi\jobs
 */
class DownloadThumbnailJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $thumbnailId;
    public $thumbnailPath;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $thumbnail = Thumbnail::find()->where(['id' => $this->thumbnailId])->one();

        if ($thumbnail === null) {
            return;
        }

        $fileName = explode('/', $thumbnail->url);
        $fileName = end($fileName);

        if (strrpos($fileName, '.') === false) {
            $fileName .= '.jpg';
        }

        $mimeType = FileHelper::getMimeTypeByExtension($fileName);
        $filegroup = Module::getLoadedModule()->getGetFilegroupByPath($this->thumbnailPath ?? 'Twitter');

        $fileType = Filetype::find()->where(['mime_type' => $mimeType])->one();

        if ($fileType === null) {
            Yii::error('Unknown mime type. Thumbnail id: ' . $thumbnail->id . ' Mime type: '. $mimeType);
            return;
        }

        $file = new File([
            'filename' => substr($fileName, 0, strrpos($fileName, '.')),
            'filetype_id' => $fileType->id,
            'filegroup_id' => $filegroup->id,
            'size' => 0,
            'is_public' => 1,
        ]);

        FileHelper::createDirectory(File::getFileTarget($file));

        if (!$file->save()) {
            Yii::error('Cannot save file. Thumbnail id: ' . $thumbnail->id . ' Error: '. $file->getErrors());
            return;
        }

        $file = File::find()->where(['id' => $file->id])->one();

        $filePath = File::getFilePath($file);

        $client = new Client();
        $res = $client->get($thumbnail->url, [
            'sink' => $filePath,
        ]);

        if ($res->getStatusCode() === 200) {
            $file->size = filesize($filePath);

            if (!$file->save()) {
                Yii::error('Cannot update file size. Thumbnail id: ' . $thumbnail->id . ' Error: '. $file->getErrors());
                $file->delete();
                return;
            }

            $thumbnail->file_id = $file->id;

            if (!$thumbnail->save()) {
                Yii::error('Cannot update thumbnail. Thumbnail id: ' . $thumbnail->id . ' Error: '. $thumbnail->getErrors());
                $file->delete();
                return;
            }
        } else {
            $file->delete();
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
