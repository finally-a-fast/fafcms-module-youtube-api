<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-youtube-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/docs Documentation of fafcms-module-youtube-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\youtubeapi\jobs;

use fafcms\fafcms\models\QueueHelper;
use fafcms\twitterapi\models\Attachment;
use fafcms\youtubeapi\models\Channel;
use fafcms\youtubeapi\models\Thumbnail;
use fafcms\youtubeapi\models\Video;
use Yii;
use fafcms\twitterapi\models\Resource;
use fafcms\twitterapi\models\Tweet;
use GuzzleHttp\Client;

/**
 * Class GetVideosJob
 *
 * @package fafcms\youtubeapi\jobs
 */
class GetVideosJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $channelId;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        try {
            $channel = Channel::find()->where(['id' => $this->channelId])->one();

            if ($channel === null) {
                return;
            }

            //$cache = new \Memcached();
            //$cache->addServer('localhost', 11211);
            //$client->setCache(new MemcachedCachePool($cache));

            $client = new \Google_Client();
            $client->setDeveloperKey($channel->youtubeuser->key);

            $youtube = new \Google_Service_YouTube($client);

            $responseContent = $youtube->playlistItems->listPlaylistItems('snippet', [
                'playlistId' => $channel->uploads,
                'maxResults' => 1000
            ]);

            foreach ($responseContent->items as $rawVideo) {
                $video = Video::find()->where(['youtube_resource_id' => $rawVideo->snippet->resourceId->videoId])->one();

                if ($video === null) {
                    $publishedAt = new \DateTime($rawVideo->snippet->publishedAt);

                    $video = new Video([
                        'project_id' => $channel->project_id,
                        'projectlanguage_id' => $channel->projectlanguage_id,
                        'channel_id' => $this->channelId,
                        'youtube_id' => $rawVideo->id,
                        'youtube_channel_id' => $rawVideo->snippet->channelId,
                        'youtube_playlist_id' => $rawVideo->snippet->playlistId,
                        'youtube_resource_id' => $rawVideo->snippet->resourceId->videoId,
                        'title' => $rawVideo->snippet->title,
                        'published_at' => $publishedAt->format('Y-m-d H:i:s'),
                    ]);

                    if (!$video->save()) {
                        Yii::error('Cannot save video. YouTube id: ' . $rawVideo->id . ' Error: '. $video->getErrors());
                    }
                }

                $rawThumbnail = Thumbnail::getBestResulutionThumbnail($rawVideo->snippet->thumbnails);

                if ($rawThumbnail === null) {
                    continue;
                }

                $thumbnail = Thumbnail::find()->where([
                    'video_id' => $video->id,
                    'url' => $rawThumbnail->url
                ])->one();

                if ($thumbnail === null) {
                    $thumbnail = new Thumbnail([
                        'project_id' => $channel->project_id,
                        'projectlanguage_id' => $channel->projectlanguage_id,
                        'video_id' => $video->id,
                        'url' => $rawThumbnail->url,
                        'height' => $rawThumbnail->height,
                        'width' => $rawThumbnail->width
                    ]);

                    if (!$thumbnail->save()) {
                        Yii::error('Cannot save thumbnail. Url: ' . $rawThumbnail->url . ' Error: '. $thumbnail->getErrors());
                        continue;
                    }
                }

                if ($channel->download_thumbnails === 1 && $thumbnail->file_id === null) {
                    QueueHelper::runJob(DownloadThumbnailJob::class, [
                        'thumbnailId' => $thumbnail->id,
                        'thumbnailPath' => $channel->thumbnail_path,
                    ]);
                }
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            throw $e;
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
