<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-youtube-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/docs Documentation of fafcms-module-youtube-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\youtubeapi\jobs;

use fafcms\fafcms\models\QueueHelper;
use fafcms\youtubeapi\models\Channel;
use fafcms\youtubeapi\models\Thumbnail;
use fafcms\youtubeapi\models\Youtubeuser;
use Yii;

/**
 * Class GetChannelsJob
 *
 * @package fafcms\youtubeapi\jobs
 */
class GetChannelsJob extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $youtubeuserId;

    /**
     * @inheritdoc
     */
    public function execute($queue)
    {
        $youtubeuser = Youtubeuser::find()->where(['id' => $this->youtubeuserId])->one();

        if ($youtubeuser === null) {
            return;
        }

        //$cache = new \Memcached();
        //$cache->addServer('localhost', 11211);
        //$client->setCache(new MemcachedCachePool($cache));

        $client = new \Google_Client();
        $client->setDeveloperKey($youtubeuser->key);

        $youtube = new \Google_Service_YouTube($client);

        $responseContent = $youtube->channels->listChannels('snippet', ['forUsername' => $youtubeuser->name]);

        foreach ($responseContent->items as $rawChannel) {
            $channel = Channel::find()->where(['youtube_id' => $rawChannel->id])->one();

            $detailsResponseContent = $youtube->channels->listChannels('contentDetails', ['id' => $rawChannel->id]);

            if (($detailsResponseContent['items'][0]['contentDetails']['relatedPlaylists'] ?? null) === null) {
                Yii::error('Cannot get channel details. YouTube id: ' . $rawChannel->id);
                continue;
            }

            if ($channel === null) {
                $publishedAt = new \DateTime($rawChannel->snippet->publishedAt);

                $channel = new Channel([
                    'project_id' => $youtubeuser->project_id,
                    'projectlanguage_id' => $youtubeuser->projectlanguage_id,
                    'youtubeuser_id' => $this->youtubeuserId,
                    'youtube_id' => $rawChannel->id,
                    'title' => $rawChannel->snippet->title,
                    'description' => $rawChannel->snippet->description,
                    'country' => $rawChannel->snippet->country,
                    'custom_url' => $rawChannel->snippet->customUrl,
                    'default_language' => $rawChannel->snippet->defaultLanguage,
                    'published_at' => $publishedAt->format('Y-m-d H:i:s'),
                    'favorites' => $detailsResponseContent['items'][0]['contentDetails']['relatedPlaylists']['favorites'],
                    'likes' => $detailsResponseContent['items'][0]['contentDetails']['relatedPlaylists']['likes'],
                    'uploads' => $detailsResponseContent['items'][0]['contentDetails']['relatedPlaylists']['uploads'],
                    'watch_history' => $detailsResponseContent['items'][0]['contentDetails']['relatedPlaylists']['watchHistory'],
                    'watch_later' => $detailsResponseContent['items'][0]['contentDetails']['relatedPlaylists']['watchLater'],
                    'download_thumbnails' => $youtubeuser->download_thumbnails,
                    'thumbnail_path' => $youtubeuser->thumbnail_path . '/' . str_replace('/', ' ', $rawChannel->snippet->title),
                    'schedule' => $youtubeuser->schedule
                ]);

                if (!$channel->save()) {
                    Yii::error('Cannot save channel. YouTube id: ' . $rawChannel->id . ' Error: '. $channel->getErrors());
                    continue;
                }
            }

            $rawThumbnail = Thumbnail::getBestResulutionThumbnail($rawChannel->snippet->thumbnails);

            if ($rawThumbnail === null) {
                continue;
            }

            $thumbnail = Thumbnail::find()->where(['url' => $rawThumbnail->url])->one();

            if ($thumbnail === null) {
                $thumbnail = new Thumbnail([
                    'project_id' => $youtubeuser->project_id,
                    'projectlanguage_id' => $youtubeuser->projectlanguage_id,
                    'channel_id' => $channel->id,
                    'url' => $rawThumbnail->url,
                    'height' => $rawThumbnail->height,
                    'width' => $rawThumbnail->width
                ]);

                if (!$thumbnail->save()) {
                    Yii::error('Cannot save thumbnail. Url: ' . $rawThumbnail->url . ' Error: '. $thumbnail->getErrors());
                    continue;
                }
            }

            if ($channel->download_thumbnails === 1 && $thumbnail->file_id === null) {
                QueueHelper::runJob(DownloadThumbnailJob::class, [
                    'thumbnailId' => $thumbnail->id,
                    'thumbnailPath' => $channel->thumbnail_path,
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getTtr()
    {
        return 3 * (60 * 60);
    }

    /**
     * @inheritdoc
     */
    public function canRetry($attempt, $error)
    {
        return $attempt < 3;
    }
}
