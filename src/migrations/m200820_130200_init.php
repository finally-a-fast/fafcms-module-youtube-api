<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-youtube-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/docs Documentation of fafcms-module-youtube-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\youtubeapi\migrations;

use fafcms\youtubeapi\models\Channel;
use fafcms\youtubeapi\models\Playlist;
use fafcms\youtubeapi\models\Thumbnail;
use fafcms\youtubeapi\models\Video;
use fafcms\youtubeapi\models\Youtubeuser;
use yii\db\Migration;
use fafcms\filemanager\models\File;

/**
 * Class m200820_130200_init
 *
 * @package fafcms\sitemanager\migrations
 */
class m200820_130200_init extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;

        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable(Youtubeuser::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'name' => $this->string(255)->notNull(),
            'remarks' => $this->text()->null()->defaultValue(null),
            'download_thumbnails' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'thumbnail_path' => $this->string(255)->null()->defaultValue('YouTube Thumbnails'),
            'key' => $this->string(255)->null()->defaultValue(null),
            'schedule' => $this->string(255)->notNull()->defaultValue('@daily'),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-youtubeuser-created_by', Youtubeuser::tableName(), ['created_by'], false);
        $this->createIndex('idx-youtubeuser-updated_by', Youtubeuser::tableName(), ['updated_by'], false);
        $this->createIndex('idx-youtubeuser-activated_by', Youtubeuser::tableName(), ['activated_by'], false);
        $this->createIndex('idx-youtubeuser-deactivated_by', Youtubeuser::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-youtubeuser-deleted_by', Youtubeuser::tableName(), ['deleted_by'], false);

        $this->createTable(Channel::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'youtubeuser_id' =>  $this->integer(10)->unsigned()->notNull(),
            'status' => $this->string(255)->notNull()->defaultValue('active'),
            'youtube_id' => $this->string(255)->null()->defaultValue(null),
            'title' => $this->string(255)->notNull(),
            'description' => $this->text()->null()->defaultValue(null),
            'country' => $this->string(255)->null()->defaultValue(null),
            'custom_url' => $this->string(255)->null()->defaultValue(null),
            'default_language' => $this->string(255)->null()->defaultValue(null),
            'published_at' => $this->datetime()->null()->defaultValue(null),
            'favorites' => $this->string(255)->null()->defaultValue(null),
            'likes' => $this->string(255)->null()->defaultValue(null),
            'uploads' => $this->string(255)->null()->defaultValue(null),
            'watch_history' => $this->string(255)->null()->defaultValue(null),
            'watch_later' => $this->string(255)->null()->defaultValue(null),
            'download_thumbnails' => $this->tinyInteger(1)->unsigned()->notNull()->defaultValue(1),
            'thumbnail_path' => $this->string(255)->null()->defaultValue('YouTube Thumbnails'),
            'schedule' => $this->string(255)->notNull()->defaultValue('@daily'),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-channel-youtubeuser_id', Channel::tableName(), ['youtubeuser_id'], false);
        $this->createIndex('idx-channel-created_by', Channel::tableName(), ['created_by'], false);
        $this->createIndex('idx-channel-updated_by', Channel::tableName(), ['updated_by'], false);
        $this->createIndex('idx-channel-activated_by', Channel::tableName(), ['activated_by'], false);
        $this->createIndex('idx-channel-deactivated_by', Channel::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-channel-deleted_by', Channel::tableName(), ['deleted_by'], false);

        $this->createTable(Video::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'channel_id' =>  $this->integer(10)->unsigned()->notNull(),
            'youtube_id' => $this->string(255)->null()->defaultValue(null),
            'youtube_channel_id' => $this->string(255)->null()->defaultValue(null),
            'youtube_playlist_id' => $this->string(255)->null()->defaultValue(null),
            'youtube_resource_id' => $this->string(255)->null()->defaultValue(null),
            'title' => $this->string(255)->null()->defaultValue(null),
            'published_at' => $this->datetime()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-video-channel_id', Video::tableName(), ['channel_id'], false);
        $this->createIndex('idx-video-created_by', Video::tableName(), ['created_by'], false);
        $this->createIndex('idx-video-updated_by', Video::tableName(), ['updated_by'], false);
        $this->createIndex('idx-video-activated_by', Video::tableName(), ['activated_by'], false);
        $this->createIndex('idx-video-deactivated_by', Video::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-video-deleted_by', Video::tableName(), ['deleted_by'], false);

        $this->createTable(Thumbnail::tableName(), [
            'id' => $this->primaryKey(10)->unsigned(),
            'channel_id' =>  $this->integer(10)->unsigned()->null()->defaultValue(null),
            'video_id' =>  $this->integer(10)->unsigned()->null()->defaultValue(null),
            'file_id' =>  $this->integer(10)->unsigned()->null()->defaultValue(null),
            'url' => $this->string(255)->null()->defaultValue(null),
            'height' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'width' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'updated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'activated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deactivated_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'deleted_by' => $this->integer(10)->unsigned()->null()->defaultValue(null),
            'created_at' => $this->datetime()->null()->defaultValue(null),
            'updated_at' => $this->datetime()->null()->defaultValue(null),
            'activated_at' => $this->datetime()->null()->defaultValue(null),
            'deactivated_at' => $this->datetime()->null()->defaultValue(null),
            'deleted_at' => $this->datetime()->null()->defaultValue(null),
        ], $tableOptions);

        $this->createIndex('idx-thumbnail-channel_id', Thumbnail::tableName(), ['channel_id'], false);
        $this->createIndex('idx-thumbnail-video_id', Thumbnail::tableName(), ['video_id'], false);
        $this->createIndex('idx-thumbnail-file_id', Thumbnail::tableName(), ['file_id'], false);
        $this->createIndex('idx-thumbnail-created_by', Thumbnail::tableName(), ['created_by'], false);
        $this->createIndex('idx-thumbnail-updated_by', Thumbnail::tableName(), ['updated_by'], false);
        $this->createIndex('idx-thumbnail-activated_by', Thumbnail::tableName(), ['activated_by'], false);
        $this->createIndex('idx-thumbnail-deactivated_by', Thumbnail::tableName(), ['deactivated_by'], false);
        $this->createIndex('idx-thumbnail-deleted_by', Thumbnail::tableName(), ['deleted_by'], false);

        $this->addForeignKey('fk-channel-youtubeuser_id', Channel::tableName(), 'youtubeuser_id', Youtubeuser::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-video-channel_id', Video::tableName(), 'channel_id', Channel::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-thumbnail-channel_id', Thumbnail::tableName(), 'channel_id', Channel::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-thumbnail-video_id', Thumbnail::tableName(), 'video_id', Video::tableName(), 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-thumbnail-file_id', Thumbnail::tableName(), 'file_id', File::tableName(), 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-channel-youtubeuser_id', Channel::tableName());
        $this->dropForeignKey('fk-video-channel_id', Video::tableName());
        $this->dropForeignKey('fk-thumbnail-channel_id', Thumbnail::tableName());
        $this->dropForeignKey('fk-thumbnail-video_id', Thumbnail::tableName());
        $this->dropForeignKey('fk-thumbnail-file_id', Thumbnail::tableName());

        $this->dropTable(Youtubeuser::tableName());
        $this->dropTable(Channel::tableName());
        $this->dropTable(Video::tableName());
        $this->dropTable(Thumbnail::tableName());
    }
}
