<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2020 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-youtube-api
 * @see https://www.finally-a-fast.com/packages/fafcms-module-youtube-api/docs Documentation of fafcms-module-youtube-api
 * @since File available since Release 1.0.0
 */

namespace fafcms\youtubeapi;

use fafcms\fafcms\inputs\Chips;
use fafcms\fafcms\inputs\NumberInput;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Module
 *
 * @package fafcms\youtubeapi
 */
class Module extends PluginModule
{
}
